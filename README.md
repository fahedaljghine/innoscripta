# Innoscripta News aggregator Project Setup Guide

Follow these steps to set up the Innoscripta project on your local environment:

## Clone the Project

```bash
git clone https://gitlab.com/fahedaljghine/innoscripta.git
```

## Navigate to the Project Directory

```bash.p/../
cd innoscripta
```

## Install Composer Dependencies
if you wish you can run this batch file to do the magic
```bash
chmod +x setup.sh
./setup.sh
```

or check the set up instructions below
## Install Composer Dependencies

```bash
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```

## Copy the Environment File

```bash
cp .env.example .env
```

Update the configurations in the `.env` file based on your local environment.

## Start Laravel Sail

```bash
./vendor/bin/sail up
```

This command may take some time, especially the first time you run it, as it downloads the Docker images and sets up the containers.

## Generate Application Key

```bash
./vendor/bin/sail artisan key:generate
```

## Run Migrations

```bash
./vendor/bin/sail artisan migrate --seed
```

## Visit the App

Visit the application URL at [localhost](http://localhost).

Now your Innoscripta project should be up and running on your local machine!


## Databases Implementation

The mission doesn't explicitly specify the database type. However, for the purpose of demonstrating skills and different approaches, two database solutions have been implemented:

### MySQL Database

The MySQL database structure includes migrations, models, and repositories.

- **Migrations:** Migrations are used to create tables and define their structure within the MySQL database.
- **Models:** Models represent the data structure and relationships, offering a way to interact with the database entities.
- **Repositories:** Repositories provide an interface for interacting with the MySQL database, including CRUD operations.


### NoSQL Database ([SleekDB](https://sleekdb.github.io))

The main and preferred solution is the NoSQL implementation using SleekDB. This database solution aligns well with the nature of the articles and offers flexibility for unstructured data. It's designed to efficiently manage and store article data, presenting an effective NoSQL solution.

SleekDB allows for:

- A flexible schema and management of unstructured data.
- Efficient data storage and retrieval without the constraints of a structured schema.
- Streamlined operations and manipulation of NoSQL data.

The SleekDB implementation caters directly to the task's requirements and is the primary solution used within this application.

Both database implementations demonstrate the ability to manage and organize data in different database types, showcasing the thought process behind selecting the most appropriate database solution for the specific context of the task.

#### You can change the DB from env file
```ARTICLE_STORAGE_DB=``` can be ```nosql, mysql``` or form the ``config/news.php`` file


## Configuration for News Sources

In the `config/news.php` file, you have the flexibility to choose which news sources to enable for the fetching process. This configuration file allows you to toggle specific sources on or off for data retrieval. Here's an example structure of the configuration file:

```php
return [
    'sources' => [
        'bbcNews' => false, // Enable to fetch from BBC News
        'newsApi' => true,  // Enable to fetch from NewsAPI
        'newsApiOrg' => true,  // Enable to fetch from NewsAPI.org
        'newsCred' => false, // Enable to fetch from NewsCred
        'newYorkTimes' => false, // Enable to fetch from New York Times
        'openNews' => false, // Enable to fetch from OpenNews
        'theGuardian' => true, // Enable to fetch from The Guardian
    ],

    /*
     * choose what type of database you want to store fetched articles
     * nosql , mysql
    */
    'articlesStorageDB' => env('ARTICLE_STORAGE_DB', 'mysql'),
];
```
Please note that only some sources implemented in this task. Other sources are included in the configuration for illustrative purposes but are not fully implemented. This configuration file allows for the selective activation of news sources for the data fetching process.

###### Implemented sources 

- newsApi // Event Registry aka NewsApi
- newsApiOrg // NewsApi.org
- theGuardian // The guardian


## Schedule
In `Console/Kernel.php`, we've scheduled the `articles:all` command to run every minute. However, just setting the schedule isn't sufficient; 
we need to inform the operating system to execute the cron job. To achieve this, run `./vendor/bin/sail artisan schedule:run`. 
I've made it more convenient by adjusting the docker file to handle this within the container, eliminating the need for an additional configuration on your end. 
So, no further setup is necessary from your side.

### TODO

- implement the remaining clients
- implement tests
- implement pagination when fetching articles
- dig deep in configurations, every client has many configurations and features and can manipulated based on the business need

## 3rd party packages used

- spatie/laravel-fractal
- rakibtg/sleekdb
- pestphp/pest
