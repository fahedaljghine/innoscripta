<?php

use App\Models\Preference;
use App\Models\User;
use Laravel\Sanctum\Sanctum;

it('can return unauthenticated', function () {

    Sanctum::actingAs(new User());

    $response = $this->get('/api/user');

    $response->assertStatus(401);
});

it('can retrieve user info and preference', function () {

    $user = User::factory()->create();

    Preference::factory(5)
        ->create(['user_id' => $user]);

    Sanctum::actingAs(
        $user
    );

    $response = $this->get('/api/user');

    $response->assertOk();
    $data = $response->json()['data'];
    expect($data['email'])->not->toBeNull();
    expect($data['preferences'])->not->toBeNull();
});
