<?php

use App\Models\Article;
use App\Models\Category;
use App\Models\Preference;
use App\Models\User;
use Laravel\Sanctum\Sanctum;


it('can retrieve articles based on user preferences', function () {

    $user = User::factory()->create();

    Preference::factory(5)
        ->create(['user_id' => $user]);

    Sanctum::actingAs(
        $user
    );

    $response = $this->get('/api/articles', [
        'category' => Category::where('name', 'news')->first()?->id
    ]);

    expect(Article::count())->toBeGreaterThanOrEqual(0);

    $response->assertOk();
    $data = $response->json()['data'];
    expect($data)->not->toBeNull();
});
