<?php

namespace Tests\Unit\Repositories;

use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

it('can find a category by name', function () {
    // Create a test category
    Category::factory()->create(['name' => 'Test']);

    $categoryRepository = app(CategoryRepositoryInterface::class);
    // Call the findByName method
    $foundCategory = $categoryRepository->findByName('Test');

    // Assert that the category was found
    expect($foundCategory)->not->toBeNull();
    expect($foundCategory->name)->toEqual('Test');
});

it('returns null if category not found by name', function () {

    $categoryRepository = app(CategoryRepositoryInterface::class);
    // Call the findByName method with a name that doesn't exist
    $foundCategory =$categoryRepository->findByName('Nonexistent');

    // Assert that null is returned
    expect($foundCategory)->toBeNull();
});
