<?php

namespace Database\Factories;

use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Relations\Relation;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class PreferenceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $type = fake()->randomElement(
            [
                'category',
                'source',
                'author'
            ]
        );

        return [
            'user_id' => User::all()->random()->id,
            'model_type' => $type,
            'model_id' => Relation::$morphMap[$type]::all()->random()->id,
        ];
    }
}
