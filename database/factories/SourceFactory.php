<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Source>
 */
class SourceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name,
            'slug' => fake()->slug,
            'description' => fake()->paragraph(3),
            'url' => fake()->url,
            'category_id' => Category::all()->random()->id,
            'language' => fake()->languageCode,
            'country' => fake()->countryCode
        ];
    }
}
