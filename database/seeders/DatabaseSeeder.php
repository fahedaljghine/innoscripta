<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Category;
use App\Models\Preference;
use App\Models\Source;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Category::factory(10)->create();
        Author::factory(10)->create();
        Source::factory(10)->create();
        User::factory(10)->create();

        Preference::factory(30)->create();


        $testUser = User::where('email', 'user@innoscripta.com')->first();

        if (is_null($testUser)) {
            $testUser = User::factory()->create([
                'name' => 'Test User',
                'email' => 'user@innoscripta.com',
            ]);
        }

        Preference::factory(5)->create([
            'user_id' => $testUser->id
        ]);

        $this->call(SourcesSeeder::class);
    }
}
