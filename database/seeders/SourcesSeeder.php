<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Source;
use App\Repositories\Interfaces\SourceRepositoryInterface;
use App\Services\News\NewsAPIOrg\NewsAPIOrgInterface;
use App\Traits\Logger;
use App\Transformers\Clients\NewsAPIOrgTransformer\Models\SourceTransformer;
use Illuminate\Database\Seeder;

class SourcesSeeder extends Seeder
{
    use Logger;

    protected $sourceTransformer;

    /**
     * Run the database seeds.
     * @param NewsAPIOrgInterface $newsAPIOrg
     * @param SourceTransformer $sourceTransformer
     * @param SourceRepositoryInterface $sourceRepository
     */
    public function run(NewsAPIOrgInterface $newsAPIOrg,
                        SourceTransformer $sourceTransformer,
                        SourceRepositoryInterface $sourceRepository): void
    {

        $this->sourceTransformer = $sourceTransformer;

        $category = Category::firstOrCreate([
            'name' => 'news'
        ]);

        //the guardian
        Source::factory()->create([
            'name' => 'The Guardian',
            'slug' => 'theguardian',
            'description' => 'The Guardian is a British daily newspaper. It was founded in 1821 as The Manchester Guardian, before it changed its name in 1959. Along with its sister papers, The Observer and The Guardian Weekly, The Guardian is part of the Guardian Media Group, owned by the Scott Trust Limited.',
            'url' => 'https://www.theguardian.com/international',
            'category_id' => $category->id,
            'language' => 'en',
            'country' => 'uk',
            'provider' => 'theguardian',
        ]);


        //newsAPIOrg sources
        try {
            $results = $newsAPIOrg->fetchSources();
            $formattedSources = $this->formatSources($results['sources']);
            $sourceRepository->insert($formattedSources);
        } catch (\Exception $exception) {
            $this->log($exception);
        }
    }

    private function formatSources($sources)
    {
        $formattedSources = [];
        foreach ($sources as $source) {
            $formattedSources[] = $this->sourceTransformer->transformWithoutArticles($source);
        }

        return $formattedSources;
    }
}
