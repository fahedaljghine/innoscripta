<?php

namespace App\Providers;

use App\Models\Author;
use App\Models\Category;
use App\Models\Source;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //Prevent team members from implementing any (n+1) problem
        Model::shouldBeStrict($this->app->environment('local'));

        //Map relations for morph
        Relation::morphMap([
            'category' => Category::class,
            'source' => Source::class,
            'author' => Author::class,
        ]);
    }
}
