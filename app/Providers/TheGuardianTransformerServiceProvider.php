<?php

namespace App\Providers;


use App\Transformers\Clients\TheGuardianTransformer\Interfaces\ArticleTransformerInterface;
use App\Transformers\Clients\TheGuardianTransformer\Interfaces\AuthorTransformerInterface;
use App\Transformers\Clients\TheGuardianTransformer\Interfaces\SourceTransformerInterface;
use App\Transformers\Clients\TheGuardianTransformer\Models\ArticleTransformer;
use App\Transformers\Clients\TheGuardianTransformer\Models\AuthorTransformer;
use App\Transformers\Clients\TheGuardianTransformer\TheGuardianTransformer;
use App\Transformers\Clients\TheGuardianTransformer\TheGuardianTransformerInterface;
use Illuminate\Support\ServiceProvider;


class TheGuardianTransformerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TheGuardianTransformerInterface::class, TheGuardianTransformer::class);
        $this->app->bind(ArticleTransformerInterface::class, ArticleTransformer::class);
        $this->app->bind(AuthorTransformerInterface::class, AuthorTransformer::class);
    }

    public function boot()
    {
        // Additional setup or actions after services are registered
    }
}
