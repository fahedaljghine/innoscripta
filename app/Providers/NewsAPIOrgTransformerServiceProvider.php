<?php

namespace App\Providers;


use App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces\ArticleTransformerInterface;
use App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces\AuthorTransformerInterface;
use App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces\SourceTransformerInterface;
use App\Transformers\Clients\NewsAPIOrgTransformer\Models\ArticleTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\Models\AuthorTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\Models\SourceTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\NewsAPIOrgTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\NewsAPIOrgTransformerInterface;
use Illuminate\Support\ServiceProvider;


class NewsAPIOrgTransformerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NewsAPIOrgTransformerInterface::class, NewsAPIOrgTransformer::class);
        $this->app->bind(ArticleTransformerInterface::class, ArticleTransformer::class);
        $this->app->bind(AuthorTransformerInterface::class, AuthorTransformer::class);
        $this->app->bind(SourceTransformerInterface::class, SourceTransformer::class);
    }

    public function boot()
    {
        // Additional setup or actions after services are registered
    }
}
