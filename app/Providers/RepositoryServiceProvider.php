<?php

namespace App\Providers;

use App\Repositories\Interfaces\ArticleRepositoryInterface;
use App\Repositories\Interfaces\ArticleSleekRepositoryInterface;
use App\Repositories\Interfaces\AuthorRepositoryInterface;
use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\PreferenceRepositoryInterface;
use App\Repositories\Interfaces\SleekDBRepositoryInterface;
use App\Repositories\Interfaces\SourceRepositoryInterface;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Models\ArticleRepository;
use App\Repositories\Models\ArticleSleekRepository;
use App\Repositories\Models\AuthorRepository;
use App\Repositories\Models\BaseRepository;
use App\Repositories\Models\CategoryRepository;
use App\Repositories\Models\PreferenceRepository;
use App\Repositories\Models\SleekDBRepository;
use App\Repositories\Models\SourceRepository;
use App\Repositories\Models\UserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(ArticleRepositoryInterface::class, ArticleRepository::class);
        $this->app->bind(SourceRepositoryInterface::class, SourceRepository::class);
        $this->app->bind(AuthorRepositoryInterface::class, AuthorRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(PreferenceRepositoryInterface::class, PreferenceRepository::class);

        $this->app->bind(SleekDBRepositoryInterface::class, SleekDBRepository::class);
        $this->app->bind(ArticleSleekRepositoryInterface::class, ArticleSleekRepository::class);
    }

    public function boot()
    {
        // Additional setup or actions after services are registered
    }
}
