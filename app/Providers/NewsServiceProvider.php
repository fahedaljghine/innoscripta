<?php

namespace App\Providers;

use App\Services\News\BaseBaseNewsClient;
use App\Services\News\BaseNewsClientInterface;
use App\Services\News\NewsAPI\NewsAPIClient;
use App\Services\News\NewsAPI\NewsAPIInterface;
use App\Services\News\NewsAPIOrg\NewsAPIOrgClient;
use App\Services\News\NewsAPIOrg\NewsAPIOrgInterface;
use App\Services\News\TheGuardian\TheGuardianClient;
use App\Services\News\TheGuardian\TheGuardianInterface;
use App\Transformers\BaseNewsTransformer;
use App\Transformers\BaseNewsTransformerInterface;
use Illuminate\Support\ServiceProvider;

class NewsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BaseNewsClientInterface::class, BaseBaseNewsClient::class);
        $this->app->bind(NewsAPIInterface::class, NewsAPIClient::class);
        $this->app->bind(NewsAPIOrgInterface::class, NewsAPIOrgClient::class);
        $this->app->bind(TheGuardianInterface::class, TheGuardianClient::class);

        $this->app->bind(BaseNewsTransformerInterface::class, BaseNewsTransformer::class);
    }

    public function boot()
    {
        // Additional setup or actions after services are registered
    }
}
