<?php

namespace App\Providers;


use App\Transformers\Clients\NewsAPITransformer\Interfaces\ArticleTransformerInterface;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\AuthorTransformerInterface;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\SourceTransformerInterface;
use App\Transformers\Clients\NewsAPITransformer\Models\ArticleTransformer;
use App\Transformers\Clients\NewsAPITransformer\Models\AuthorTransformer;
use App\Transformers\Clients\NewsAPITransformer\Models\SourceTransformer;
use App\Transformers\Clients\NewsAPITransformer\NewsAPITransformer;
use App\Transformers\Clients\NewsAPITransformer\NewsAPITransformerInterface;
use Illuminate\Support\ServiceProvider;


class NewsAPITransformerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NewsAPITransformerInterface::class, NewsAPITransformer::class);
        $this->app->bind(ArticleTransformerInterface::class, ArticleTransformer::class);
        $this->app->bind(AuthorTransformerInterface::class, AuthorTransformer::class);
        $this->app->bind(SourceTransformerInterface::class, SourceTransformer::class);
    }

    public function boot()
    {
        // Additional setup or actions after services are registered
    }
}
