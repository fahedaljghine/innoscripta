<?php

namespace App\Transformers;

interface BaseNewsTransformerInterface
{

    /**
     * transform to array
     *
     * @param $data
     * @return array
     */
    public function transform($data);
}
