<?php

namespace App\Transformers\Clients\TheGuardianTransformer;

interface TheGuardianTransformerInterface
{
    public function resolveSource();
}
