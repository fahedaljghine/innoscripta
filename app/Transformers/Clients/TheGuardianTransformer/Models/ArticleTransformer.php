<?php

namespace App\Transformers\Clients\TheGuardianTransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\TheGuardianTransformer\Interfaces\ArticleTransformerInterface;
use Carbon\Carbon;

class  ArticleTransformer extends BaseNewsTransformer implements ArticleTransformerInterface
{
    public function transform($datum)
    {
        return [
            'title' => $datum['webTitle'] ?? null,
            'body' => $datum['fields']['body'] ?? null,
            'url' => $datum['webUrl'] ?? null,
            'dateTime' => Carbon::parse($datum['webPublicationDate'])->format('Y-m-d H:i:s'),
            'lang' => $datum['lang'] ?? 'en',
            'image' => $datum['fields']['thumbnail'] ?? null,
            'category' => $datum['sectionName'] ?? null
        ];
    }
}
