<?php

namespace App\Transformers\Clients\TheGuardianTransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\TheGuardianTransformer\Interfaces\AuthorTransformerInterface;

class AuthorTransformer extends BaseNewsTransformer implements AuthorTransformerInterface
{
    public function transform($datum)
    {
        if (isset($datum['fields']['byline'])) {
            return [
                [
                    'name' => $datum['fields']['byline'],
                    'type' => $datum['pillarName'] ?? null,
                    'isAgency' => false,
                ]
            ];
        }

        return [];
    }
}
