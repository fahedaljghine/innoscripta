<?php

namespace App\Transformers\Clients\TheGuardianTransformer;

use App\Models\Source;
use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\TheGuardianTransformer\Interfaces\ArticleTransformerInterface;
use App\Transformers\Clients\TheGuardianTransformer\Interfaces\AuthorTransformerInterface;

class TheGuardianTransformer extends BaseNewsTransformer implements TheGuardianTransformerInterface
{
    protected $articleTransformer;
    protected $authorTransformer;
    protected $source;

    public function __construct(
        ArticleTransformerInterface $articleTransformer,
        AuthorTransformerInterface $authorTransformer
    )
    {
        $this->articleTransformer = $articleTransformer;
        $this->authorTransformer = $authorTransformer;
        $this->source = $this->resolveSource();
    }

    public function transform($data)
    {
        $articles = [];
        foreach ($data['response']['results'] as $datum) {
            $articles[] = $this->transformArticle($datum);
        }

        return $articles;
    }

    private function transformArticle($datum)
    {
        $authors = $this->authorTransformer->transform($datum);
        $article = $this->articleTransformer->transform($datum);


        $article['authors'] = $authors;
        $article['source'] = $this->formatSource();

        return $article;
    }

    public function resolveSource()
    {
        return Source::where('slug', 'theguardian')
            ->with('category')
            ->first();
    }

    public function formatSource()
    {
        $source = $this->source
            ->toArray();
        $source['category'] = $this->source->category->name;
        return $source;
    }
}
