<?php

namespace App\Transformers\Clients\NewsAPITransformer;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\ArticleTransformerInterface;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\AuthorTransformerInterface;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\SourceTransformerInterface;

class NewsAPITransformer extends BaseNewsTransformer implements NewsAPITransformerInterface
{
    private $articleTransformer;
    private $sourceTransformer;
    private $authorTransformer;


    public function __construct(
        ArticleTransformerInterface $articleTransformer,
        SourceTransformerInterface $sourceTransformer,
        AuthorTransformerInterface $authorTransformer
    )
    {
        $this->articleTransformer = $articleTransformer;
        $this->sourceTransformer = $sourceTransformer;
        $this->authorTransformer = $authorTransformer;
    }

    public function transform($data)
    {
        $articles = [];

        foreach ($data['recentActivityArticles']['activity'] as $datum) {
            $articles[] = $this->transformArticle($datum);
        }

        return $articles;
    }

    private function transformArticle($datum)
    {
        $source = $this->sourceTransformer->transform($datum['source']);
        $authors = $this->authorTransformer->transform($datum['authors']);
        $article = $this->articleTransformer->transform($datum);

        $article['authors'] = $authors;
        $article['source'] = $source;

        return $article;
    }
}
