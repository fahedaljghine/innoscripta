<?php

namespace App\Transformers\Clients\NewsAPITransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\ArticleTransformerInterface;
use Carbon\Carbon;

class  ArticleTransformer extends BaseNewsTransformer implements ArticleTransformerInterface
{
    public function transform($datum)
    {
        return [
            'title' => $datum['title'] ?? null,
            'body' => $datum['body'] ?? null,
            'url' => $datum['url'] ?? null,
            'dateTime' => Carbon::parse($datum['dateTime'])->format('Y-m-d H:i:s'),
            'lang' => $datum['lang'] ?? 'en',
            'image' => $datum['image'] ?? null,
            'category' => $datum['dataType'] ?? null
        ];
    }
}
