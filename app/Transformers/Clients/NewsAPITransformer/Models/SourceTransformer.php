<?php

namespace App\Transformers\Clients\NewsAPITransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\SourceTransformerInterface;

class SourceTransformer extends BaseNewsTransformer implements SourceTransformerInterface
{
    public function transform($datum)
    {
        return [
            'url' => $datum['uri'] ?? null,
            'category' => $datum['dataType'] ?? null,
            'name' => $datum['title'] ?? null,
            'country' => $datum['location']['country']['label']['eng'] ?? null,
            'provider' => 'newsApi',
        ];
    }

}
