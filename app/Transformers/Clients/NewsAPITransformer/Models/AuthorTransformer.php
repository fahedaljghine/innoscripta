<?php

namespace App\Transformers\Clients\NewsAPITransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPITransformer\Interfaces\AuthorTransformerInterface;

class AuthorTransformer extends BaseNewsTransformer implements AuthorTransformerInterface
{
    public function transform($datum)
    {
        $authors = [];
        foreach ($datum as $item) {
            $authors[] = [
                'url' => $item['uri'] ?? null,
                'name' => $item['name'] ?? null,
                'type' => $item['type'] ?? null,
                'isAgency' => $item['isAgency'] ?? null,
            ];
        }

        return $authors;
    }
}
