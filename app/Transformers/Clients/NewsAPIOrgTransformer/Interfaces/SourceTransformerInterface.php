<?php

namespace App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces;


interface SourceTransformerInterface
{
    public function transformWithoutArticles($item);
}
