<?php

namespace App\Transformers\Clients\NewsAPIOrgTransformer\Models;

use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces\SourceTransformerInterface;

class SourceTransformer extends BaseNewsTransformer implements SourceTransformerInterface
{
    protected $categoryRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function transform($datum)
    {
        return [
            'slug' => $datum['id'] ?? null,
            'name' => $datum['name'] ?? null,
            'provider' => 'newsApiOrg',
            'category' => 'news'
        ];
    }

    public function transformWithoutArticles($item)
    {
        return [
            'slug' => $item['id'] ?? null,
            'name' => $item['name'] ?? null,
            'description' => $item['description'] ?? null,
            'url' => $item['url'] ?? null,
            'category_id' => $this->resolveCategory($item['category'] ?? null),
            'language' => $item['language'] ?? null,
            'country' => $item['country'] ?? null,
            'provider' => 'newsApiOrg',
        ];
    }

    protected function resolveCategory($name)
    {
        return $this->categoryRepository
            ->firstOrCreate([
                'name' => $name
            ])
            ->id;
    }
}
