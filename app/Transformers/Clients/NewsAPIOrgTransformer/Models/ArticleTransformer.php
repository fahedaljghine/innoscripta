<?php

namespace App\Transformers\Clients\NewsAPIOrgTransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces\ArticleTransformerInterface;
use Carbon\Carbon;

class ArticleTransformer extends BaseNewsTransformer implements ArticleTransformerInterface
{
    public function transform($datum)
    {
        return [
            'title' => $datum['title'] ?? null,
            'body' => $datum['description'] ?? '',
            'url' => $datum['url'] ?? null,
            'dateTime' => Carbon::parse($datum['publishedAt'])->format('Y-m-d H:i:s'),
            'lang' => $datum['lang'] ?? 'en',
            'image' => $datum['urlToImage'] ?? null,
            'category' => 'news',
        ];
    }
}
