<?php

namespace App\Transformers\Clients\NewsAPIOrgTransformer\Models;

use App\Transformers\BaseNewsTransformer;
use App\Transformers\Clients\NewsAPIOrgTransformer\Interfaces\AuthorTransformerInterface;

class AuthorTransformer extends BaseNewsTransformer implements AuthorTransformerInterface
{
    public function transform($datum)
    {
        if (isset($datum['author'])) {
            return [
                [
                    'name' => $datum['author'],
                    'type' => 'news',
                ]
            ];
        }

        return [];
    }
}
