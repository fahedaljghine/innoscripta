<?php

namespace App\Console\Commands\Contracts;

interface FetchArticlesCommandInterface
{
    public function fetchArticles();

    public function formatArticles($articles);

    public function persistArticles($articles);

    public function persistArticlesMySql($articles);

    public function persistArticlesNoSql($articles);
}
