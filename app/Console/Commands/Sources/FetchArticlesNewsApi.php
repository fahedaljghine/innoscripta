<?php

namespace App\Console\Commands\Sources;

use App\Console\Commands\FetchArticlesBase;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use App\Repositories\Interfaces\ArticleSleekRepositoryInterface;
use App\Services\News\NewsAPI\NewsAPIInterface;
use App\Transformers\Clients\NewsAPITransformer\NewsAPITransformerInterface;

class FetchArticlesNewsApi extends FetchArticlesBase
{

    /**
     * Execute the command
     * @param NewsAPIInterface $newsAPI
     * @param NewsAPITransformerInterface $newsAPITransformer
     * @param ArticleRepositoryInterface $articleRepository
     * @param ArticleSleekRepositoryInterface $articleSleekRepository
     */
    public function __construct(
        NewsAPIInterface $newsAPI,
        NewsAPITransformerInterface $newsAPITransformer,
        ArticleRepositoryInterface $articleRepository,
        ArticleSleekRepositoryInterface $articleSleekRepository
    )
    {
        parent::__construct();
        $this->newsProvider = $newsAPI;
        $this->newsProviderTransformer = $newsAPITransformer;
        $this->articleRepository = $articleRepository;
        $this->articleSleekRepository = $articleSleekRepository;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:newsApi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fetch articles from NewsAPI';

}
