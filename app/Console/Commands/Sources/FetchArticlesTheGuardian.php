<?php

namespace App\Console\Commands\Sources;

use App\Console\Commands\FetchArticlesBase;
use App\Repositories\Interfaces\ArticleSleekRepositoryInterface;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use App\Services\News\TheGuardian\TheGuardianInterface;
use App\Transformers\Clients\TheGuardianTransformer\TheGuardianTransformerInterface;

class FetchArticlesTheGuardian extends FetchArticlesBase
{

    /**
     * Execute the command
     * @param TheGuardianInterface $theGuardian
     * @param TheGuardianTransformerInterface $theGuardianTransformer
     * @param ArticleRepositoryInterface $articleRepository
     * @param ArticleSleekRepositoryInterface $articleSleekRepository
     */
    public function __construct(
        TheGuardianInterface $theGuardian,
        TheGuardianTransformerInterface $theGuardianTransformer,
        ArticleRepositoryInterface $articleRepository,
        ArticleSleekRepositoryInterface $articleSleekRepository
    )
    {
        parent::__construct();
        $this->newsProvider = $theGuardian;
        $this->newsProviderTransformer = $theGuardianTransformer;
        $this->articleRepository = $articleRepository;
        $this->articleSleekRepository = $articleSleekRepository;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:theGuardian';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fetch articles from The Guardian';

}
