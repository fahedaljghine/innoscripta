<?php

namespace App\Console\Commands\Sources;

use App\Console\Commands\FetchArticlesBase;
use App\Repositories\Interfaces\ArticleSleekRepositoryInterface;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use App\Services\News\NewsAPIOrg\NewsAPIOrgInterface;
use App\Transformers\Clients\NewsAPIOrgTransformer\NewsAPIOrgTransformerInterface;

class FetchArticlesNewsApiOrg extends FetchArticlesBase
{

    /**
     * Execute the command
     * @param NewsAPIOrgInterface $newsAPIOrg
     * @param NewsAPIOrgTransformerInterface $newsAPIOrgTransformer
     * @param ArticleRepositoryInterface $articleRepository
     * @param ArticleSleekRepositoryInterface $articleSleekRepository
     */
    public function __construct(
        NewsAPIOrgInterface $newsAPIOrg,
        NewsAPIOrgTransformerInterface $newsAPIOrgTransformer,
        ArticleRepositoryInterface $articleRepository,
        ArticleSleekRepositoryInterface $articleSleekRepository
    )
    {
        parent::__construct();
        $this->newsProvider = $newsAPIOrg;
        $this->newsProviderTransformer = $newsAPIOrgTransformer;
        $this->articleRepository = $articleRepository;
        $this->articleSleekRepository = $articleSleekRepository;
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:newsApiOrg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fetch articles from NewsAPI.org';

    public function handle()
    {

        parent::handle();
    }

}
