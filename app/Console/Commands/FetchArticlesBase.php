<?php

namespace App\Console\Commands;

use App\Console\Commands\Contracts\FetchArticlesCommandInterface;
use App\Traits\Logger;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Exception;

class FetchArticlesBase extends Command implements FetchArticlesCommandInterface
{
    use Logger;

    protected $newsProvider;
    protected $newsProviderTransformer;
    protected $articleRepository;
    protected $articleSleekRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:base';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is the base command for fetching articles, but its not runnable by its won';

    /**
     * Execute the command
     */
    public function handle()
    {
        //skip if its ran by its own
        if (is_null($this->newsProvider))
            return false;

        try {
            $this->persistArticles($this->formatArticles($this->fetchArticles()));

        } catch (Exception $exception) {
            $this->log($exception);
            $this->error('An error occurred while fetching or inserting data : ' . $exception->getMessage());
        }
    }

    public function fetchArticles()
    {
        //fetch articles from source
        $articles = $this->newsProvider->fetchArticles();
        Log::info($articles);
        $this->info('Articles fetched successfully.');

        return $articles;
    }

    public function formatArticles($articles)
    {
        //transform result into app format
        $formattedArticles = $this->newsProviderTransformer->transform($articles);
        $this->info('Articles transformed successfully.');

        return $formattedArticles;
    }

    public function persistArticles($articles)
    {
        config('news.articlesStorageDB') == 'mysql' ?
            $this->persistArticlesMySql($articles) :
            $this->persistArticlesNoSql($articles);
    }

    public function persistArticlesMySql($articles)
    {
        Log::info('persistArticlesMySql');

        //persist data into local no sql database
        $this->articleRepository->insert($articles);
        $this->info('Articles inserted successfully.');
    }

    public function persistArticlesNoSql($articles)
    {
        Log::info('persistArticlesNoSql');

        //persist data into local no sql database
        $this->articleSleekRepository->insertMany($articles);
        $this->info('Articles inserted successfully.');
    }
}
