<?php

namespace App\Console\Commands;

use App\Services\Commands\CommandExecuteService;
use Illuminate\Console\Command;

class FetchArticles extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'articles:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will fetch articles from all selected implemented sources';

    /**
     * Execute the command
     * @param CommandExecuteService $commandExecuteService
     */
    public function handle(CommandExecuteService $commandExecuteService)
    {
        foreach (array_keys(array_filter(config('news.sources'))) as $source) {
            $this->info('running articles:' . $source);
            $commandExecuteService->commandExecute($this->resolveCommandName($source));
            $this->info('finish running articles:' . $source);
        }
    }

    private function resolveCommandName($source)
    {
        return 'articles:' . $source;
    }
}
