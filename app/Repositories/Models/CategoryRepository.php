<?php

namespace App\Repositories\Models;

use App\Models\Category;
use App\Repositories\Interfaces\CategoryRepositoryInterface;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * CategoryRepository constructor.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        parent::__construct($category);
    }


    /**
     * @param string $name
     */
    public function findByName(string $name = 'news')
    {
        $this->model->where('name', 'like', '%' . $name . '%')
            ->first();
    }

}
