<?php

namespace App\Repositories\Models;

use App\Models\Preference;
use App\Repositories\Interfaces\PreferenceRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class PreferenceRepository extends BaseRepository implements PreferenceRepositoryInterface
{
    /**
     * PreferenceRepository constructor.
     *
     * @param Preference $user
     */
    public function __construct(Preference $user)
    {
        parent::__construct($user);
    }

    public function findByUserAndType($userId, $modelType): Collection
    {
        return $this->model->where('user_id', $userId)
            ->where('model_type', $modelType)
            ->with('model')
            ->get();
    }
}
