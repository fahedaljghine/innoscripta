<?php

namespace App\Repositories\Models;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        parent::__construct($user);
    }

    /**
     * @param $id
     * @return User
     */
    public function findWithRelations($id): ?User
    {
       return $this->model->where('id', $id)
            ->with('preferences', 'preferences.model')
            ->first();
    }
}
