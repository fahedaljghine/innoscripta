<?php

namespace App\Repositories\Models;

use App\Models\Source;
use App\Repositories\Interfaces\BulkInsertRepositoryInterface;
use App\Repositories\Interfaces\SourceRepositoryInterface;

class SourceRepository extends BaseRepository implements SourceRepositoryInterface, BulkInsertRepositoryInterface
{
    /**
     * SourceRepository constructor.
     *
     * @param Source $source
     */
    public function __construct(Source $source)
    {
        parent::__construct($source);
    }

    public function getAllSourcesNames($provider = null)
    {
        $query = $this->model::query();

        if (isset($provider)) {
            $query->where('provider', 'like', '%$provider%');
        }

        return $query->pluck('slug')->toArray();
    }


    public function getRandomXSourcesNames(string $provider = null, int $x)
    {
        $query = $this->model::query();

        if (isset($provider)) {
            $query->where('provider', 'like', '%' . $provider . '%');
        }

        $query->limit($x)
            ->offset(rand(1, 10));

        return $query->pluck('slug')->toArray();

    }

    public function insert(array $records): bool
    {
        return $this->model->insert($records);
    }
}
