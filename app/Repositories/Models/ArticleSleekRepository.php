<?php

namespace App\Repositories\Models;

use App\Repositories\Interfaces\ArticleSleekRepositoryInterface;

class ArticleSleekRepository extends SleekDBRepository implements ArticleSleekRepositoryInterface
{
    public function __construct()
    {
        parent::__construct('articles');
    }

    public function findAllByCriteria(array $criteria)
    {
        // TODO: Implement finAllByCriteria() method.
    }
}
