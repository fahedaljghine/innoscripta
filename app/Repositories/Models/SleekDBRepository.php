<?php

namespace App\Repositories\Models;

use SleekDB\Exceptions\InvalidArgumentException;
use SleekDB\Exceptions\InvalidConfigurationException;
use SleekDB\Exceptions\IOException;
use SleekDB\Store;

class SleekDBRepository
{
    protected $store;

    public function __construct($storeName)
    {
        try {
            $this->store = new store($storeName, 'storage/app/data');
        } catch (IOException $e) {
        } catch (InvalidArgumentException $e) {
        } catch (InvalidConfigurationException $e) {
        }
    }

    public function findAll()
    {
        return $this->store->fetch();
    }

    public function findBy($field, $value)
    {
        return $this->store->where($field, '=', $value)->fetch();
    }

    public function insertMany(array $data)
    {
        $this->store->insertMany($data);
    }

    public function insert(array $data)
    {
        $this->store->insert($data);
    }

}
