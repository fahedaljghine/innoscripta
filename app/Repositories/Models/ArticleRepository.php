<?php

namespace App\Repositories\Models;

use App\Models\Article;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use App\Repositories\Interfaces\AuthorRepositoryInterface;
use App\Repositories\Interfaces\BulkInsertRepositoryInterface;
use App\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Repositories\Interfaces\SourceRepositoryInterface;
use App\Traits\Logger;
use Illuminate\Contracts\Database\Eloquent\Builder;

class ArticleRepository extends BaseRepository implements ArticleRepositoryInterface, BulkInsertRepositoryInterface
{

    use Logger;

    protected $categoryRepository;
    protected $sourceRepository;
    protected $authorRepository;

    /**
     * ArticleRepository constructor.
     *
     * @param Article $article
     * @param CategoryRepositoryInterface $categoryRepository
     * @param SourceRepositoryInterface $sourceRepository
     * @param AuthorRepositoryInterface $authorRepository
     */
    public function __construct(Article $article,
                                CategoryRepositoryInterface $categoryRepository,
                                SourceRepositoryInterface $sourceRepository,
                                AuthorRepositoryInterface $authorRepository
    )
    {
        parent::__construct($article);

        $this->categoryRepository = $categoryRepository;
        $this->sourceRepository = $sourceRepository;
        $this->authorRepository = $authorRepository;
    }

    public function insert(array $records): bool
    {
        try {
            collect($records)->map(function ($article) {
                $this->persistItem($article);
            });
            return true;
        } catch (\Exception $exception) {
            $this->log($exception);
            return false;
        }
    }

    private function persistItem($article)
    {
        $article = $this->persistArticleCategory($article);
        $article = $this->persistSourceCategory($article);
        $article = $this->persistSource($article);
        $authorsIdes = $this->persistAuthors($article);
        $persistedArticle = $this->persistArticle($article);
        $persistedArticle->authors()->attach($authorsIdes, ['created_at' => now(), 'updated_at' => now()]);
    }

    private function persistArticleCategory($article)
    {
        $article['category_id'] = $this->categoryRepository
            ->firstOrCreate(['name' => $article['category']])->id;

        unset($article['category']);

        return $article;
    }

    private function persistSourceCategory($article)
    {
        $article['source']['category_id'] = $this->categoryRepository
            ->firstOrCreate(['name' => $article['source']['category']])->id;

        unset($article['source']['category']);

        return $article;
    }

    private function persistSource($article)
    {
        $article['source_id'] = $this->sourceRepository
            ->firstOrCreate($article['source'])->id;

        unset($article['source']);

        return $article;
    }

    private function persistArticle($article)
    {
        return $this
            ->create($article);
    }

    private function persistAuthors(&$article)
    {
        $authorsIdes = [];
        foreach ($article['authors'] as $author) {
            $authorsIdes[] = $this->authorRepository
                ->insertGetId($author);
        }
        unset($article['authors']);

        return $authorsIdes;
    }

    public function findAllByCriteria(array $criteria)
    {
        $query = $this->buildQuery($criteria);

        return $query->simplePaginate(10);
    }

    protected function buildQuery(array $criteria): Builder
    {
        $query = $this->model::query();

        $query = $this->applyTitleAndBodySearch($query, $criteria['query'] ?? null);
        $query = $this->applyDateFilter($query, $criteria['date'] ?? null);
        $query = $this->applyCategoryFilter($query, $criteria['categories'] ?? null);
        $query = $this->applySourceFilter($query, $criteria['sources'] ?? null);
        $query =  $this->applyAuthorFilter($query, $criteria['authors'] ?? null);

        return $query;
    }

    protected function applyTitleAndBodySearch(Builder $query, ?string $searchTerm): Builder
    {
        if ($searchTerm) {
            $query->where(function (Builder $query) use ($searchTerm) {
                $query->where('title', 'like', "%$searchTerm%")
                    ->orWhere('body', 'like', "%$searchTerm%");
            });
        }

        return $query;
    }

    protected function applyDateFilter(Builder $query, ?string $date): Builder
    {
        if ($date) {
            $query->where('dateTime', '>=', $date);
        }

        return $query;
    }

    protected function applyCategoryFilter(Builder $query, ?array $categories): Builder
    {
        if ($categories) {
            $query->whereIn('category_id', $categories);
        }

        return $query;
    }

    protected function applySourceFilter(Builder $query, ?array $sources): Builder
    {
        if ($sources) {
            $query->whereIn('source_id', $sources);
        }

        return $query;
    }

    protected function applyAuthorFilter(Builder $query, ?array $authors): Builder
    {
        if ($authors) {
            $query->whereHas('authors', function (Builder $query) use ($authors) {
                $query->whereIn('author_id', $authors);
            });
        }

        return $query;
    }
}
