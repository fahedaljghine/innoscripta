<?php

namespace App\Repositories\Models;

use App\Models\Author;
use App\Repositories\Interfaces\BulkInsertRepositoryInterface;
use App\Repositories\Interfaces\InsertWithIdRepositoryInterface;
use App\Repositories\Interfaces\AuthorRepositoryInterface;

class AuthorRepository extends BaseRepository implements AuthorRepositoryInterface, BulkInsertRepositoryInterface, InsertWithIdRepositoryInterface
{
    /**
     * AuthorRepository constructor.
     *
     * @param Author $author
     */
    public function __construct(Author $author)
    {
        parent::__construct($author);
    }

    /**
     * @param array $records
     *
     * @return bool
     */

    public function insert(array $records): bool
    {
        return $this->model->insert($records);
    }

    /**
     * @param array $attributes
     *
     * @return int
     */

    public function insertGetId(array $attributes): int
    {
        return $this->model->insertGetId($attributes);
    }
}
