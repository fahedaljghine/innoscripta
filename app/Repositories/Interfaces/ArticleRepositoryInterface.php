<?php
namespace App\Repositories\Interfaces;

interface ArticleRepositoryInterface
{
   public function findAllByCriteria(array $criteria);
}
