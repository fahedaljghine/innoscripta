<?php
namespace App\Repositories\Interfaces;

interface SourceRepositoryInterface
{
  public function getAllSourcesNames(string $provider);

  public function getRandomXSourcesNames(string $provider , int $x);
}
