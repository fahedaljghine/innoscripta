<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface PreferenceRepositoryInterface
{
    /**
     * @param $userId
     * @param $modelType
     * @return Collection
     */
    public function findByUserAndType($userId , $modelType): Collection;
}
