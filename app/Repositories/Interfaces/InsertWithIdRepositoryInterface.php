<?php

namespace App\Repositories\Interfaces;

interface InsertWithIdRepositoryInterface
{
    /**
     * @param array $attributes
     *
     * @return int
     */
    public function insertGetId(array $attributes): int;

}
