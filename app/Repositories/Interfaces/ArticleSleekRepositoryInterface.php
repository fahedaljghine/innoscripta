<?php
namespace App\Repositories\Interfaces;

interface ArticleSleekRepositoryInterface
{
    public function findAllByCriteria(array $criteria);
}
