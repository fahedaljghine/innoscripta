<?php

namespace App\Repositories\Interfaces;

interface SleekDBRepositoryInterface
{
    public function findAll();

    public function findBy($field, $value);

    public function bulkInsert(array $data);

    public function insert(array $data);
}
