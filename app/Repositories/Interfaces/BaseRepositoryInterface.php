<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

//Base repository contract
interface BaseRepositoryInterface
{
    /**
     * @param array $attributes
     * @return Model
     */
    public function create(array $attributes): Model;


    /**
     * @param array $attributes
     * @return Model
     */
    public function firstOrCreate(array $attributes): Model;

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model;


    /**
     * @return Collection
     */
    public function all(): Collection;
}
