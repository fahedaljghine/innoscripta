<?php

namespace App\Repositories\Interfaces;


interface BulkInsertRepositoryInterface
{
    /**
     * @param array $records
     *
     * @return bool
     */
    public function insert(array $records): bool;

}
