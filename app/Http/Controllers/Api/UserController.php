<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Traits\HasApiResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    use HasApiResponse;

    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUser(Request $request)
    {
        $user = $this->userRepository
            ->findWithRelations($request->user()->id);

        if ($user) {
            return $this->formatResponse(new UserResource($user));
        }

        return $this->notFoundResponse('User not found!');
    }
}
