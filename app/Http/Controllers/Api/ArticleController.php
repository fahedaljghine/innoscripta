<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleResource;
use App\Repositories\Interfaces\ArticleRepositoryInterface;
use App\Repositories\Interfaces\ArticleSleekRepositoryInterface;
use App\Repositories\Interfaces\PreferenceRepositoryInterface;
use App\Services\CriteriaResolver;
use App\Traits\HasApiResponse;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    use HasApiResponse;

    protected $articleRepository;
    protected $articleSleekRepository;
    protected $preferenceRepository;
    protected $criteriaResolver;

    public function __construct(
        ArticleRepositoryInterface $articleRepository,
        ArticleSleekRepositoryInterface $articleSleekRepository,
        PreferenceRepositoryInterface $preferenceRepository,
        CriteriaResolver $criteriaResolver
    )
    {
        $this->articleRepository = $articleRepository;
        $this->articleSleekRepository = $articleSleekRepository;
        $this->preferenceRepository = $preferenceRepository;
        $this->criteriaResolver = $criteriaResolver;
    }

    public function getArticles(Request $request)
    {
        $articles = $this->getArticlesByCriteria($request);

        return $this->formatResponse(ArticleResource::collection($articles));
    }

    private function getArticlesByCriteria(Request $request)
    {
        $criteria = $this->criteriaResolver->resolveCriteria($request);

        return config('news.articlesStorageDB') == 'mysql' ?
            $this->articleRepository->findAllByCriteria($criteria) :
            $this->articleSleekRepository->findAllByCriteria($criteria);
    }
}
