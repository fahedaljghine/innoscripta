<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'type',
        'isAgency',
        'url',
    ];


    public function articles()
    {
        return $this->belongsToMany(Author::class, 'article_authors', 'author_id', 'article_id');
    }

    public function preferences()
    {
        return $this->morphMany(Preference::class, 'model', 'model_type', 'model_id');
    }
}
