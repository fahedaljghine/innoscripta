<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'description',
        'url',
        'category_id',
        'language',
        'country',
        'provider',
    ];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function preferences()
    {
        return $this->morphMany(Preference::class, 'model', 'model_type', 'model_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
