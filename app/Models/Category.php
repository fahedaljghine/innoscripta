<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
    ];

    public function preferences()
    {
        return $this->morphMany(Preference::class, 'model', 'model_type', 'model_id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function sources()
    {
        return $this->hasMany(Source::class);
    }
}
