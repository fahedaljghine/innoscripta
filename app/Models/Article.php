<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'body',
        'image',
        'lang',
        'dateTime',
        'url',
        'source_id',
        'category_id',
    ];


    protected $casts = [
        'dateTime' => 'timestamp',
    ];

    protected $dates = [
        'dateTime'
    ];


    public function source()
    {
        return $this->belongsTo(Source::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function authors(){
        return $this->belongsToMany(Author::class , 'article_authors' , 'article_id' , 'author_id');
    }
}
