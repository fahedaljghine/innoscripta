<?php

namespace App\Traits;

trait HasApiResponse
{
    function formatResponse($data, $message = 'Success', $status = 200)
    {
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $status);
    }

    function notFoundResponse($message)
    {
        return $this->formatResponse([], $message, 401);
    }
}
