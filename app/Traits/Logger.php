<?php

namespace App\Traits;

use Illuminate\Support\Facades\Log;

trait Logger
{
    public function log($exception)
    {
        Log::error($exception->getMessage());
        Log::error($exception->getTraceAsString());
    }
}
