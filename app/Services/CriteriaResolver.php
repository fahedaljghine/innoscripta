<?php

namespace App\Services;

use App\Repositories\Interfaces\PreferenceRepositoryInterface;
use Illuminate\Http\Request;

class CriteriaResolver
{
    protected $preferenceRepository;

    public function __construct(PreferenceRepositoryInterface $preferenceRepository)
    {
        $this->preferenceRepository = $preferenceRepository;
    }

    public function resolveCriteria(Request $request)
    {
        $criteria = [
            'query' => $request->input('query'),
            'date' => $request->input('date'),
            'categories' => $this->resolveCategories($request),
            'sources' => $this->resolveSources($request),
            'authors' => $this->resolveAuthors($request),
        ];

        return array_filter($criteria, function ($value) {
            return !is_null($value);
        });
    }

    private function resolveCategories(Request $request)
    {
        $requestCategory = $request->input('category');

        $userPreferenceCategories = $this->getUserPreferences($request->user()->id,'category');

        return $this->filterAndMerge([$requestCategory], $userPreferenceCategories);
    }

    private function resolveSources(Request $request)
    {
        $requestSource = $request->input('source');

        $userPreferenceSources = $this->getUserPreferences($request->user()->id,'source');

        return $this->filterAndMerge([$requestSource], $userPreferenceSources);
    }

    private function resolveAuthors(Request $request)
    {
        $requestAuthor = $request->input('author');

        $userPreferenceAuthors = $this->getUserPreferences($request->user()->id,'author');

        return $this->filterAndMerge([$requestAuthor], $userPreferenceAuthors);
    }

    private function getUserPreferences($userId,$modelType)
    {
        return $this->preferenceRepository
            ->findByUserAndType($userId, $modelType)
            ->pluck('model_id')
            ->toArray();
    }

    private function filterAndMerge(array $requestValues, array $userPreferenceValues)
    {
        $filteredRequestValues = array_filter($requestValues, function ($value) {
            return !is_null($value);
        });

        return array_merge($userPreferenceValues, $filteredRequestValues);
    }
}
