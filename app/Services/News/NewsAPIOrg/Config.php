<?php

namespace App\Services\News\NewsAPIOrg;

class Config
{
    const PROVIDER_NAME = 'newsApiOrg';
    const BASE_URL = 'https://newsapi.org/v2/';
    const STREAM_URL = Config::BASE_URL . 'top-headlines';
    const SOURCES_URL = Config::BASE_URL . 'top-headlines/sources';
}


