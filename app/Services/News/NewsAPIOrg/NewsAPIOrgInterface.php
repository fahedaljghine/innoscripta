<?php

namespace App\Services\News\NewsAPIOrg;

interface NewsAPIOrgInterface
{
    public function resolveSources();

    public function configurationAsString();

    public function resolveSourcesUrl();

    public function fetchSources();
}
