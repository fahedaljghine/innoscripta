<?php

namespace App\Services\News\NewsAPIOrg;


use App\Repositories\Interfaces\SourceRepositoryInterface;
use App\Services\News\ArticleManagement;
use App\Services\News\BaseBaseNewsClient;
use App\Services\News\SourceManagement;
use Carbon\Carbon;

class NewsAPIOrgClient extends BaseBaseNewsClient implements NewsAPIOrgInterface
{
    protected $sourceRepository;
    protected $sourceManagement;

    public function __construct(ArticleManagement $articleManagement,
                                SourceManagement $sourceManagement,
                                SourceRepositoryInterface $sourceRepository)
    {
        parent::__construct($articleManagement);

        $this->sourceManagement = $sourceManagement;
        $this->sourceRepository = $sourceRepository;
    }

    public function fetchSources()
    {
        return $this->sourceManagement
            ->fetchSources(
                $this->resolveSourcesUrl(),
                $this->resolveConfiguration(),
                $this->resolveHeaders()
            );
    }

    public function resolveStreamUrl()
    {
        return Config::STREAM_URL;
    }

    public function resolveSourcesUrl()
    {
        return Config::SOURCES_URL;
    }

    public function resolveConfiguration(array $config = [])
    {
        return [
            'from' => $this->resolveFromDateTime(),
            'pageSize' => 100,
            'sources' => $this->resolveSources()
        ];
    }

    public function resolveHeaders()
    {
        return [
            'X-Api-Key' => config('services.newsApiOrg.apiKey')
        ];
    }

    public function resolveSources()
    {
        return implode(',', $this->sourceRepository->getRandomXSourcesNames(Config::PROVIDER_NAME , 20));
    }

    public function resolveFromDateTime()
    {
        return Carbon::now()->subMinute()->format('Y-m-dTh:i:s');
    }

    public function configurationAsString()
    {
        return http_build_query($this->resolveConfiguration());
    }
}
