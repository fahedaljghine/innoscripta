<?php

namespace App\Services\News;

interface BaseNewsClientInterface
{

    /**
     * Resolve articles stream url.
     *
     * @return string
     */
    public function resolveStreamUrl();

    /**
     * Resolve request headers.
     *
     * @return array
     */
    public function resolveHeaders();


    /**
     * Resolve config of request.
     *
     * @param array $config
     * @return array
     */
    public function resolveConfiguration(array $config);

    /**
     * Fetch all articles.
     *
     * @return array
     */
    public function fetchArticles();
}
