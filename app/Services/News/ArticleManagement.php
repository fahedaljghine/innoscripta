<?php

namespace App\Services\News;

class ArticleManagement
{
    protected $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function fetchArticles($articlesUrl, $data = [], $headers = [])
    {
        return $this->httpClient->get($articlesUrl, $data, $headers);
    }
}
