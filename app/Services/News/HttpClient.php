<?php

namespace App\Services\News;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class HttpClient
{
    private function sendRequest($url, $method, $data = [], $headers = [])
    {
        $headers =
            array_merge(
                [
                    'Accept' => 'application/json',
                ],
                $headers
            );

        $response = Http::
        withHeaders($headers)
            ->{$method}($url, $data);

        if ($response->status() == 200) {
            return $response->json();
        } else {
            //TODO improve error handling
            Log::error('Failed request. Status: ' . $response->status());
            Log::error('Failed request. Status: ' . $response->body());
        }

        return false;
    }

    public function get($url, $data = [], $headers = [])
    {
        return $this->sendRequest($url, 'get', $data, $headers);
    }

    public function post($url, $data = [], $headers = [])
    {
        return $this->sendRequest($url, 'post', $data, $headers);
    }
}
