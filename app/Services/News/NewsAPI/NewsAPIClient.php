<?php

namespace App\Services\News\NewsAPI;


use App\Services\News\BaseBaseNewsClient;

class NewsAPIClient extends BaseBaseNewsClient implements NewsAPIInterface
{
    public function resolveStreamUrl()
    {
        return Config::STREAM_URL;
    }

    public function resolveConfiguration(array $config = [])
    {
        return [
            //this depends on the bossiness who fast we need the recent news
            'recentActivityArticlesUpdatesAfterMinsAgo' => 1,
            //how many articles in every batch we need
            'recentActivityArticlesMaxArticleCount' => 100,
            'articleBodyLen' => -1,
            'includeArticleConcepts' => true,
            'includeArticleCategories' => true,
            'apiKey' => config('services.newsApi.apiKey')
        ];
    }
}
