<?php

namespace App\Services\News\TheGuardian;


use App\Services\News\BaseBaseNewsClient;

class TheGuardianClient extends BaseBaseNewsClient implements TheGuardianInterface
{
    public function resolveStreamUrl()
    {
        return Config::STREAM_URL;
    }

    public function resolveConfiguration(array $config = [])
    {
        return [
            'page-size' => 100,
            'show-fields' => 'byline,body,thumbnail'
        ];
    }

    public function resolveHeaders()
    {
        return [
            'api-key' => config('services.theGuardian.apiKey')
        ];
    }
}
