<?php

namespace App\Services\News;

class SourceManagement
{
    protected $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function fetchSources($sourcesUrl, $data = [], $headers = [])
    {
        return $this->httpClient->get($sourcesUrl, $data, $headers);
    }
}
