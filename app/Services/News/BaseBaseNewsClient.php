<?php

namespace App\Services\News;

use Carbon\Carbon;

class BaseBaseNewsClient implements BaseNewsClientInterface
{
    protected $articleManagement;

    public function __construct(ArticleManagement $articleManagement)
    {
        $this->articleManagement = $articleManagement;
    }


    public function fetchArticles()
    {
        return $this->articleManagement
            ->fetchArticles(
                $this->resolveStreamUrl(),
                $this->resolveConfiguration(),
                $this->resolveHeaders()
            );
    }

    public function resolveStreamUrl()
    {
        return null;
    }

    public function resolveConfiguration(array $config = [])
    {
        return [];
    }

    public function resolveFromDateTime()
    {
        return Carbon::now()->subMinute();
    }

    public function resolveHeaders()
    {
        return [];
    }
}
