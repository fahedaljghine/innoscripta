<?php

namespace App\Services\Commands;

use Illuminate\Support\Facades\Artisan;

class CommandExistenceService
{
    /**
     * @param string $command
     * @return bool
     */
    public function commandExists(string $command): bool
    {
        return array_key_exists($command, Artisan::all());
    }
}
