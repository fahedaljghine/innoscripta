<?php

namespace App\Services\Commands;

use Illuminate\Support\Facades\Artisan;

class CommandExecuteService
{
    protected $commandExistenceService;

    public function __construct(CommandExistenceService $commandExistenceService)
    {
        $this->commandExistenceService = $commandExistenceService;
    }

    /**
     * @param string $command
     * @return bool
     */
    public function commandExecute(string $command): bool
    {
        if ($this->commandExistenceService->commandExists($command)) {
            return Artisan::call($command);
        }

        return '';
    }
}
