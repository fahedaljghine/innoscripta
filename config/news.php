<?php


//here we can select which of the implemented sources to fetch from
//the commented items are not implemented yet

return [
    'sources' => [
        // Configuration to select which news sources to fetch from
        'bbcNews' => false, // Enable to fetch from BBC News
        'newsApi' => true,  // Enable to fetch from NewsAPI
        'newsApiOrg' => false,  // Enable to fetch from NewsAPI.org
        'newsCred' => false, // Enable to fetch from NewsCred
        'newYorkTimes' => false, // Enable to fetch from New York Times
        'openNews' => false, // Enable to fetch from OpenNews
        'theGuardian' => true, // Enable to fetch from The Guardian
    ],

    /*
     * choose what type of database you want to store fetched articles
     * nosql , mysql
    */
    'articlesStorageDB' => env('ARTICLE_STORAGE_DB', 'mysql'),
];

